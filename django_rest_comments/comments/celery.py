# -*- coding: utf-8 -*-
"""
Created on 21.12.16

@author: status
"""
from __future__ import absolute_import

from celery import Celery
from django.conf import settings
import os


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_rest_comments.settings')

app = Celery('comments',
             broker='amqp://',
             backend=settings.BROKER_URL,
             include=['comments.tasks'])

# Optional configuration, see the application user guide.
app.conf.update(
    CELERY_TASK_RESULT_EXPIRES=3600,
)

if __name__ == '__main__':
    app.start()
