# -*- coding: utf-8 -*-
"""
Created on 18.12.16

@author: status
"""
from rest_framework import renderers

class XMLFileRenderer(renderers.BaseRenderer):
    media_type = 'application/xml'
    format = 'xml'

    def render(self, data, media_type=None, renderer_context=None):
        file = open(data, 'rb')
        return file
