from django.test import TestCase

from comments.models import Comment
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User


class CommentTests(TestCase):
    """
    Comment model tests
    """
    def setUp(self):
        """
        tree comments

        user test
            comment_1
                comment_2
                    comment_3
                    comment_5
                comment_6
            comment_4
        """

        author = User.objects.create(username='test')
        Comment.objects.create(
            id=1,
            content_type=ContentType.objects.get_for_model(User),
            object_id=1,
            author=author,
            content='Root 1 - user 1 - level 1'
        )
        Comment.objects.create(
            id=2,
            content_type=ContentType.objects.get_for_model(Comment),
            object_id=1,
            author=author,
            content='Root 1 - comment 1 - level 2'
        )
        Comment.objects.create(
            id=3,
            content_type=ContentType.objects.get_for_model(Comment),
            object_id=2,
            author=author,
            content='Root 1 - comment 2 - level 3'
        )
        Comment.objects.create(
            id=4,
            content_type=ContentType.objects.get_for_model(User),
            object_id=1,
            author=author,
            content='Root 2 - user 1 - level 1'
        )
        Comment.objects.create(
            id=5,
            content_type=ContentType.objects.get_for_model(Comment),
            object_id=2,
            author=author,
            content='Root 1 - user 1 - level 3'
        )
        Comment.objects.create(
            id=6,
            content_type=ContentType.objects.get_for_model(Comment),
            object_id=1,
            author=author,
            content='Root 1 - user 1 - level 2'
        )

    def test_has_childs(self):
        comment_1 = Comment.objects.get(pk=1)
        self.assertTrue(comment_1.has_childs())
        comment_2 = Comment.objects.get(pk=2)
        self.assertTrue(comment_2.has_childs())
        comment_3 = Comment.objects.get(pk=3)
        self.assertFalse(comment_3.has_childs())
        comment_4 = Comment.objects.get(pk=4)
        self.assertFalse(comment_4.has_childs())

    def test_cant_delete_has_child(self):
        comment_1 = Comment.objects.get(pk=1)
        with self.assertRaises(Comment.HasChildException):
            comment_1.delete()
        comment_2 = Comment.objects.get(pk=2)
        with self.assertRaises(comment_2.HasChildException):
            comment_2.delete()
        comment_4 = Comment.objects.get(pk=4)
        comment_4.delete()
        with self.assertRaises(Comment.DoesNotExist):
            Comment.objects.get(pk=4)

    def test_get_childs_first_level(self):
        comment_1 = Comment.objects.get(pk=1)
        self.assertQuerysetEqual(
            comment_1.get_childs_first_level(),
            ['<Comment: test: 2-1>', '<Comment: test: 6-1>']
        )
        self.assertEqual(comment_1.get_childs_first_level().count(), 2)
        comment_3 = Comment.objects.get(pk=3)
        self.assertQuerysetEqual(
            comment_3.get_childs_first_level(),
            Comment.objects.none()
        )
        self.assertEqual(comment_3.get_childs_first_level().count(), 0)
