# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_delete
from django.core.cache import cache
from comments.utils import get_timestamp, conn_to_psycopg2
import psycopg2


class HasChildException(Exception):
    def __init__(self):
        self.message = 'Can not delete a comment, because it has children'


class Comment(models.Model):
    HasChildException = HasChildException
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    created = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, db_index=True)
    content = models.TextField()

    class Meta:
        ordering = ('created',)
        index_together = [
            ['content_type', 'object_id']
        ]

    def __unicode__(self):
        return u'%s: %s-%s' % (self.author, self.pk, self.object_id)

    def delete(self, using=None, keep_parents=False):
        if self.has_childs():
            if ContentType.objects.get_for_model(self) == self.content_type \
                    and self.id == self.object_id:
                return super(Comment, self).delete()
            else:
                raise self.HasChildException
        return super(Comment, self).delete()

    def has_childs(self):
        return Comment.objects.filter(
            content_type=ContentType.objects.get_for_model(self),
            object_id=self.pk
        ).exists()

    def get_childs_first_level(self):
        childs = Comment.objects.filter(
            content_type=ContentType.objects.get_for_model(Comment),
            object_id=self.id
        )
        if childs.exists():
            return childs.order_by('created')
        return Comment.objects.none()

    def get_childs_pg_recursive(self):
        comment_ct_id = ContentType.objects.get_for_model(Comment).id
        conn = conn_to_psycopg2()
        dict_cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        sql_params = {
            'pk': self.id,
            'ct_comment_id': comment_ct_id
        }

        sql_request = """
            WITH RECURSIVE r AS (
               SELECT id, object_id, content_type_id, created, edited, content,
               author_id, 1 as level
               FROM comments_comment
               WHERE object_id = %(pk)d and content_type_id = %(ct_comment_id)d

               UNION

               SELECT comments_comment.id, comments_comment.object_id,
               comments_comment.content_type_id, comments_comment.created,
               comments_comment.edited, comments_comment.content,
               comments_comment.author_id, r.level + 1 AS level
               FROM comments_comment
                  JOIN r
                      ON comments_comment.object_id = r.id
            )

            SELECT * FROM r
                WHERE r.content_type_id = %(ct_comment_id)d
                    ORDER BY level, created ASC;
        """ % sql_params

        dict_cur.execute(sql_request)
        return dict_cur.fetchall()

    @staticmethod
    def get_tree(comments, start=None, end=None):
        comment_ct_id = ContentType.objects.get_for_model(Comment).id
        conn = conn_to_psycopg2()
        dict_cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        list_trees = []

        for comment in comments:
            between = ''
            if start and end:
                between = """
                AND created BETWEEN '%s' AND '%s'
                """ % (start.strftime('%Y-%m-%d %H:%M:%S'),
                       end.strftime('%Y-%m-%d %H:%M:%S'))

            sql_params = {
                'ct_comment_id': comment_ct_id,
                'between': between,
                'pk': comment.pk,
                'ct_item_id': comment.content_type_id
            }

            sql_request = """
                WITH RECURSIVE r AS (
                   SELECT id, object_id, content_type_id, created, edited,
                   content, author_id, 1 as level
                   FROM comments_comment
                   WHERE object_id = %(pk)d
                       and content_type_id = %(ct_comment_id)d

                   UNION

                   SELECT comments_comment.id, comments_comment.object_id,
                   comments_comment.content_type_id, comments_comment.created,
                   comments_comment.edited, comments_comment.content,
                   comments_comment.author_id, r.level + 1 AS level
                   FROM comments_comment
                      JOIN r
                          ON comments_comment.object_id = r.id
                )

                SELECT * FROM r WHERE r.content_type_id = %(ct_comment_id)d
                    %(between)s

                UNION
                   SELECT id, object_id, content_type_id, created, edited,
                   content, author_id, 1 as level
                   FROM comments_comment
                   WHERE id = %(pk)d and content_type_id = %(ct_item_id)d %(between)s
                   ORDER BY level, created ASC
            """ % sql_params

            dict_cur.execute(sql_request)
            list_trees.append(dict_cur.fetchall())
        return list_trees


@receiver(post_save, sender=Comment)
def valid_child(instance, **kwargs):
    if instance.content_type == ContentType.objects.get_for_model(Comment) and \
                    instance.id == instance.object_id:
        comment = Comment(
            object_id=instance.object_id,
            content_type=instance.content_type,
            author=instance.author,
            content=instance.content,
        )
        instance.delete()
        comment.save()


class Loads(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    created = models.DateTimeField(auto_now_add=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    file = models.FileField(upload_to='author_history_loads')
    file_ext = models.TextField(max_length=50)

    def __unicode__(self):
        return u'%s-%s-%s' % (self.content_type_id, self.object_id,
                              self.created)


@receiver(post_save, sender=Loads)
def on_change(instance, **kwargs):
    start = 0
    if instance.start_date:
        start = get_timestamp(instance.start_date)
    end = 0
    if instance.end_date:
        end = get_timestamp(instance.end_date)
    cache_key = 'loads:%s:%s:%s:%s:%s' % \
                (instance.object_id, instance.content_type_id,
                 instance.file_ext, start, end)
    cache.set(cache_key, instance.file.path)


@receiver(pre_delete, sender=Loads)
def on_delete(instance, **kwargs):
    start = 0
    if instance.start_date:
        start = get_timestamp(instance.start_date)
    end = 0
    if instance.end_date:
        end = get_timestamp(instance.end_date)
    cache_key = 'loads:%s:%s:%s:%s:%s' % \
                (instance.object_id, instance.content_type_id,
                 instance.file_ext, start, end)
    cache.delete(cache_key)
