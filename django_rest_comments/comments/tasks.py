# -*- coding: utf-8 -*-
"""
Created on 21.12.16

@author: status
"""
from comments.celery import app
from comments.models import Loads
from django.core.files.base import ContentFile


@app.task(ignore_result=True)
def create_loads(data):
    file_data = data.pop('file')
    loads = Loads(
        content_type_id=data.get('content_type_id'),
        object_id=data.get('object_id'),
        start_date=data.get('start_date'),
        end_date=data.get('end_date'),
        file_ext=data.get('file_ext'),
    )
    loads.file.save('comments.xml', ContentFile(file_data))
    loads.save()
