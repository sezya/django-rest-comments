from django.contrib.contenttypes.models import ContentType
from django.shortcuts import Http404, HttpResponse
from wsgiref.util import FileWrapper
from django.core.cache import cache
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework import generics

from comments.serializers import CommentSerializer, LoadsSerializer, \
    CommentEditSerializer
from comments.models import Comment, Loads
from comments.tasks import create_loads

from datetime import datetime
from lxml import etree


class CommentList(mixins.ListModelMixin, mixins.CreateModelMixin,
                  generics.GenericAPIView):
    """
    List first level comments for object or create new comment for object
    """
    serializer_class = CommentSerializer

    def get_queryset(self, *args, **kwargs):
        content_type_id = self.kwargs.get('content_type_id')
        object_id = self.kwargs.get('object_id')
        comments = Comment.objects.filter(content_type_id=content_type_id,
                                          object_id=object_id)
        return comments

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        content_type_id = self.kwargs.get('content_type_id')
        object_id = self.kwargs.get('object_id')
        request.data['object_id'] = object_id
        request.data['content_type'] = content_type_id
        return self.create(request, *args, **kwargs)


class CommentDetail(mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin, generics.GenericAPIView):
    serializer_class = CommentEditSerializer
    queryset = Comment.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        try:
            return self.destroy(request, *args, **kwargs)
        except Comment.HasChildException as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)


class CommentObjectList(mixins.ListModelMixin, generics.GenericAPIView):
    """
    List first level comments for Object
    """
    serializer_class = CommentSerializer

    def get_queryset(self, *args, **kwargs):
        content_type_id = self.kwargs.get('content_type_id')
        object_id = self.kwargs.get('object_id')
        if not object_id or not content_type_id:
            raise Http404
        comments = Comment.objects.filter(content_type_id=content_type_id,
                                          object_id=object_id)
        return comments

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


@api_view(['GET'])
def comment_childs(request, pk):
    """
    Retrieve comment childs
    """
    try:
        comment = Comment.objects.get(pk=pk)
    except Comment.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        childs = comment.get_childs_pg_recursive()

        return Response(childs)


@api_view(['GET'])
def comment_tree(request, pk, content_type_id=None):
    """
    Retrieve comment tree
    """
    if content_type_id:
        comment_content_type = ContentType.objects.get_for_model(Comment)
        if comment_content_type.id == int(content_type_id):
            comments = Comment.objects.filter(pk=pk)
        else:
            comments = Comment.objects.filter(
                object_id=pk,
                content_type_id=content_type_id
            )
    else:
        comments = Comment.objects.filter(pk=pk)
    if not comments.exists():
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        tree = Comment.get_tree(comments)
        return Response(tree)


class CommentUser(generics.ListAPIView):
    serializer_class = CommentSerializer

    def get_queryset(self):
        author_id = self.kwargs.get('author_id')
        comments = Comment.objects.filter(author_id=author_id)
        if comments.exists():
            return comments.order_by('created')
        else:
            return Comment.objects.none()


@api_view(['GET'])
def comment_author(request, author_id):
    """
    Retrieve comment for user
    """
    comments = Comment.objects.filter(author_id=author_id)
    serializer = CommentSerializer(comments, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def comment_author_file(request, author_id, file_ext, start=0, end=0):
    """
    Download comment for user
    """
    content_type = ContentType.objects.get_for_model(User)
    cache_key = 'loads:%s:%s:%s:%s:%s' % (author_id, content_type.id, file_ext,
                                          start, end)
    cache_file = cache.get(cache_key)
    support_ext = ['xml']
    if file_ext not in support_ext:
        return Response("%s don't support format file" % file_ext,
                        status=status.HTTP_400_BAD_REQUEST)
    if cache_file:
        file_data = open(cache_file, 'rb')
        response = HttpResponse(FileWrapper(file_data),
                                content_type='application/xml')
        response['Content-Disposition'] = 'attachment; filename=comments.xml'
        return response
    else:
        comments = Comment.objects.filter(author_id=author_id)
        if start:
            start = datetime.utcfromtimestamp(float(start))
        else:
            start = None
        if end:
            end = datetime.utcfromtimestamp(float(end))
        else:
            end = None
        if start and end and start >= end:
            return Response("Error in dates",
                            status=status.HTTP_400_BAD_REQUEST)
        elif start < end:
            comments = comments.filter(created__range=(start, end))
        elif (start and not end) or (not start and end):
            return Response("Error in dates",
                            status=status.HTTP_400_BAD_REQUEST)

        if file_ext == 'xml':
            root = etree.Element('root')
            root.set('author', author_id)
            root.set('loaded', datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            for i in comments:
                item = etree.SubElement(root, 'item')
                item.set('id', str(i.id))
                item.set('object_id', str(i.object_id))
                item.set('created', i.created.strftime('%Y-%m-%d %H:%M:%S'))
                item.set('edited', i.edited.strftime('%Y-%m-%d %H:%M:%S'))
                item.set('content_type', str(i.content_type_id))
                item.text = i.content
            xml = etree.tostring(root)
            data = {
                'content_type_id': content_type.id,
                'object_id': author_id,
                'start_date': start,
                'end_date': end,
                'file_ext': file_ext,
                'file': xml
            }
            create_loads.delay(data)
            response = HttpResponse(xml, content_type='application/xml')
            response['Content-Disposition'] = 'attachment; ' \
                                              'filename=comments.xml'
            return response


@api_view(['GET'])
def comment_file(request, pk, content_type_id, file_ext, start=0, end=0):
    """
    Download comment for user
    """
    cache_key = 'loads:%s:%s:%s:%s:%s' % \
                (pk, content_type_id, file_ext, start, end)
    cache_file = cache.get(cache_key)
    support_ext = ['xml']
    if file_ext not in support_ext:
        return Response("%s don't support format file" % file_ext,
                        status=status.HTTP_400_BAD_REQUEST)
    if cache_file:
        file_data = open(cache_file, 'rb')
        response = HttpResponse(FileWrapper(file_data),
                                content_type='application/xml')
        response['Content-Disposition'] = 'attachment; filename=comments.xml'
        return response
    else:
        comments = Comment.objects.filter(object_id=pk,
                                          content_type_id=content_type_id)
        if start:
            start = datetime.utcfromtimestamp(float(start))
        else:
            start = None
        if end:
            end = datetime.utcfromtimestamp(float(end))
        else:
            end = None
        if start and end:
            if start >= end:
                return Response("Error in dates",
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                tree = Comment.get_tree(comments, start, end)
        else:
            tree = Comment.get_tree(comments)

        if file_ext == 'xml':
            root = etree.Element('root')
            root.set('conetent_type', str(content_type_id))
            root.set('pk', str(pk))
            for t in tree:
                for i in t:
                    item = etree.SubElement(root, 'item')
                    item.set('id', str(i.get('id')))
                    item.set('object_id', str(i.get('object_id')))
                    item.set('created', i.get('created').strftime('%Y-%m-%d %H:%M:%S'))
                    item.set('edited', i.get('edited').strftime('%Y-%m-%d %H:%M:%S'))
                    item.set('content_type', str(i.get('content_type_id')))
                    item.set('level', str(i.get('level')))
                    item.set('author', str(i.get('author_id')))
                    item.text = i.get('content')
            xml = etree.tostring(root)
            data = {
                'content_type_id': content_type_id,
                'object_id': pk,
                'start_date': start,
                'end_date': end,
                'file_ext': file_ext,
                'file': xml
            }
            create_loads.delay(data)

            response = HttpResponse(xml, content_type='application/xml')
            response['Content-Disposition'] = 'attachment; ' \
                                              'filename=comments.xml'
            return response


class LoadsAuthorList(generics.ListAPIView):
    serializer_class = LoadsSerializer

    def get_queryset(self):
        author_id = self.kwargs.get('author_id')
        comments = Loads.objects.filter(
            object_id=author_id,
            content_type=ContentType.objects.get_for_model(User)
        )
        if comments.exists():
            return comments.order_by('created')
        else:
            return Comment.objects.none()
