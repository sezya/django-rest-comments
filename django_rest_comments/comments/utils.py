# -*- coding: utf-8 -*-
"""
Created on 18.12.16

@author: status
"""
from datetime import datetime
from django.conf import settings
import psycopg2


def json_serial(obj):
    """
    JSON serializer for objects not serializable by default json code
    """
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    raise TypeError("Type not serializable")


def get_timestamp(date_time):
    d_timedelta = date_time - datetime(1970, 1, 1)
    return int(d_timedelta.total_seconds())


def conn_to_psycopg2():
    conn = psycopg2.connect(
            database=settings.DATABASES['default']['NAME'],
            user=settings.DATABASES['default']['USER'],
            password=settings.DATABASES['default']['PASSWORD']
        )
    return conn
