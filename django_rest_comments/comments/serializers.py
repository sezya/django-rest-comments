# -*- coding: utf-8 -*-
"""
Created on 17.12.16

@author: status
"""
from comments.models import Comment, Loads
from rest_framework import serializers


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'


class CommentEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'
        read_only_fields  = ('object_id', 'content_type',)


class LoadsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Loads
        fields = '__all__'
