from django.conf.urls import url, include
from django.contrib import admin
from comments import views
from django.views.static import serve
from django.conf import settings


urlpatterns = [
    url(r'^comments/(?P<pk>[0-9]+)/$', views.CommentDetail.as_view()),
    url(r'^comments/(?P<content_type_id>[0-9]+)/(?P<object_id>[0-9]+)*/$',
        views.CommentList.as_view()),
    url(r'^comments/childs/(?P<pk>[0-9]+)/$', views.comment_childs),
    url(r'^comments/tree/(?P<pk>[0-9]+)/$', views.comment_tree),
    url(r'^comments/tree/(?P<content_type_id>[0-9]+)/(?P<pk>[0-9]+)/$',
        views.comment_tree),
    # url(r'^comments/author/(?P<author_id>[0-9]+)/$',
    #     views.CommentUser.as_view()),
    url(r'^comments/author/(?P<author_id>[0-9]+)/$', views.comment_author),
    url(r'^comments/author/loads/(?P<author_id>[0-9]+)/$',
        views.LoadsAuthorList.as_view()),
    url(r'^comments/author/(?P<author_id>[0-9]+)\.(?P<file_ext>\w+)$', views.comment_author_file),
    url(r'^comments/author/(?P<author_id>[0-9]+)/(?P<start>[0-9]+)/(?P<end>[0-9]+)\.(?P<file_ext>\w+)$', views.comment_author_file),
    url(r'^comments/file/(?P<content_type_id>[\.0-9]+)/(?P<pk>[\.0-9]+)\.(?P<file_ext>\w+)$', views.comment_file),
    url(r'^comments/file/(?P<content_type_id>[\.0-9]+)/(?P<pk>[\.0-9]+)/(?P<start>[0-9]+)/(?P<end>[0-9]+)\.(?P<file_ext>\w+)$', views.comment_file),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('rest_framework.urls', namespace='rest_framework')),
]

urlpatterns += [
    url(r'^media/(?P<path>.*)$', serve, {
        'document_root': settings.MEDIA_ROOT,
    }),
]
