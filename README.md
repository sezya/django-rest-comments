### 7.1. Создание комментария для сущности с ее указанием
`content_type_id` - идентификатор типа сущности

`object_id` - идентификатор сущности

URL: `/comments/{content_type_id}/{object_id}/`

Method: POST

Данные:

`content` - текст комментария

`author` - идентификатор пользователя


### 7.2. Редактирование и удаление комментария по идентификатору

`comment_id` - идентификатор комментария

URL: `/comments/{comment_id}/`

Method: PUT

Данные передаваемые в теле запроса

`content_type` - идентификатор типа сущности

`object_id` - идетнификатор сущности

`content` - текст комментария

`author` - идентификатор пользователя

удаление комментария

URL: `/comments/{comment_id}/`

Method: DELETE

просмотр комментария 

URL: `/comments/{comment_id}/`

Method: GET


### 7.3 Получение комментариев первого уровня для определенной сущности с пагинацией 
`content_type_id` - идентификатор типа сущности

`object_id` - идентификатор сущности

URL: `/comments/{content_type_id}/{object_id}/`

Method: GET


### 7.4 Получение всех дочерних комментариев без ограничения по уровню вложенности. 
`comment_id` - идентификатор комментария

URL: `/comments/childs/{сomment_id}/`

Method: GET


### 7.5. Получение полной ветки комментариев с указанием корня. Корень определяется парой значений 
`object_id` - идентификатор сущности

`content_type_id` - идентификатор типа сущности

Корень по указанию сущности

URL: `/comments/tree/{content_type_id}/{object_id}/`

Method: GET

Корень по указанию коментария

`comment_id` - идентификатор комментария

URL: /comments/tree/{comment_id}/

Method: GET


### 7.6. Получение истории комментариев определенного пользователя.

`author_id` - идентификатор пользователя

URL: `/comments/author/{author_id}/`

Method: GET


### 7.7 Выгрузка в файл (например в xml­формате) всей истории комментариев.

Выгрузка в файл комментариев пользователя с выборкой по дате

`author_id` - идентификатор пользователя

`start` - от даты

`end` - до даты

`format` - расширение файла (xml)

URL: `/comments/author/{author_id}/{start}/{end}.{format}`

Method: GET

Выгрузка в файл всех комментариев пользователя

`author_id` - идентификатор пользователя

`format` - расширение файла (xml)

URL: `/comments/author/{author_id}.{format}`

Method: GET


Выгрузка комментариев для сущности c выбор инетервала дат

`content_type_id` - идентификатор типа сущности

`object_id` - идентификтор сущности

`start` - от даты

`end` - до даты

`format` - расширение файла (xml)

URL: `/comments/file/{content_type_id_}/{object_id}/{start}/{end}.{format}`

Method: GET


Выгрузка всех комментариев для сущности

`content_type_id` - идентификатор типа сущности

`object_id` - идентификтор сущности

`format` - расширение файла (xml)

URL: `/comments/file/{content_type_id_}/{object_id}.{format}`

Method: GET
 
 
### 7.8. Просмотр   истории   всех   выгрузок   пользователя   с   возможностью   повторной  
`author_id` - идентификатор пользователя

URL: `/comments/author/loads/{author_id}/`

Method: GET
